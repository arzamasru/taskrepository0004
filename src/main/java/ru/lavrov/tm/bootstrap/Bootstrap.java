package ru.lavrov.tm.bootstrap;

import ru.lavrov.tm.entity.Project;
import ru.lavrov.tm.entity.Task;
import ru.lavrov.tm.repository.ProjectRepository;
import ru.lavrov.tm.repository.TaskRepository;
import ru.lavrov.tm.service.ProjectService;
import ru.lavrov.tm.service.TaskService;
import ru.lavrov.tm.сonstant.ConsoleConst;

import java.util.Scanner;

public class Bootstrap {
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final TaskRepository taskRepository = new TaskRepository();
    private final ProjectService projectService = new ProjectService(projectRepository, taskRepository);
    private final TaskService taskService = new TaskService(taskRepository, projectRepository);
    private final Scanner input = new Scanner(System.in);
    private final String EnterName = "enter name:";
    private final String EnterProjectName = "enter project name:";
    private final String EnterTaskName = "enter task name:";
    private final String Ok = "[ok]";
    private final String IncorrectArgs = "incorrect args";
    private final String ProjectRemove = "[project remove]";
    private final String TaskRemove = "[task remove]";
    private final String ProjectCreate = "[project create]";
    private final String TaskCreate = "[task create]";
    private final String ProjectsRemoved = "[All projects removed]";
    private final String TasksRemoved = "[All tasks removed]";
    private final String ProjectList = "[Project list]";
    private final String TaskList = "[Task list]";
    private final String TaskAttach = "[task attach]";
    private final String ProjectTasks = "[project tasks]";


    public void init() throws RuntimeException {
        boolean exitFlag = false;
        String command;

        System.out.println("*** Welcome to task manager ***");

        while (!exitFlag) {
            command = input.nextLine();

            try{
                switch (command) {
                    case ConsoleConst.HELP:
                        displayHelp();
                        break;
                    case ConsoleConst.CREATE_PROJECT:
                        createProject();
                        break;
                    case ConsoleConst.CLEAR_PROJECT:
                        clearProject();
                        break;
                    case ConsoleConst.DISPLAY_PROJECTS:
                        displayProjects();
                        break;
                    case ConsoleConst.REMOVE_PROJECT:
                        removeProject();
                        break;
                    case ConsoleConst.CLEAR_TASK:
                        clearTask();
                        break;
                    case ConsoleConst.CREATE_TASK:
                        createTask();
                        break;
                    case ConsoleConst.DISPLAY_TASK:
                        displayTasks();
                        break;
                    case ConsoleConst.REMOVE_TASK:
                        removeTask();
                        break;
                    case ConsoleConst.ATTACH_TASK:
                        attachTask();
                        break;
                    case ConsoleConst.PROJECT_TASKS:
                        displayProjectTasks();
                        break;
                    case ConsoleConst.EXIT:
                        exitFlag = true;
                        break;
                    default:
                        System.out.println(IncorrectArgs);
                        break;
                }
            } catch (RuntimeException e){
                System.out.println(e.getMessage());
            }
        }
        input.close();
        System.out.print("you left this wonderful program");
    }

    private void removeTask() throws RuntimeException {
        System.out.println(TaskRemove);
        System.out.println(EnterName);
        String command = input.nextLine();
        taskService.removeTask(command);
        System.out.println(Ok);
    }

    private void removeProject() throws RuntimeException {
        System.out.println(ProjectRemove);
        System.out.println(EnterName);
        String command = input.nextLine();
        projectService.removeProject(command);
        System.out.println(Ok);
    }

    private void createProject() throws RuntimeException {
        System.out.println(ProjectCreate);
        System.out.println(EnterName);
        String command = input.nextLine();
        projectService.persist(command);
        System.out.println(Ok);
    }

    private void createTask() throws RuntimeException {
        System.out.println(TaskCreate);
        System.out.println(EnterName);
        String command = input.nextLine();
        taskService.persist(command);
        System.out.println(Ok);
    }

    private void clearProject() {
        projectService.removeAll();
        System.out.println(ProjectsRemoved);
    }

    private void clearTask() {
        taskService.removeAll();
        System.out.println(TasksRemoved);
    }

    private void displayProjects(){
        System.out.println(ProjectList);
        for (Project project : projectService.findAll()) {
            System.out.println(project);
        }
    }

    private void displayTasks(){
        System.out.println(TaskList);
        for (Task task : taskService.findAll()) {
            System.out.println(task);
        }
    }

    public void attachTask() throws RuntimeException {
        System.out.println(TaskAttach);
        System.out.println(EnterTaskName);
        String taskName = input.nextLine();
        System.out.println(EnterProjectName);
        String projectName = input.nextLine();
        taskService.attachTask(taskName, projectName);
        System.out.println(Ok);
    }

    private void displayProjectTasks() throws RuntimeException {
        System.out.println(ProjectTasks);
        System.out.println(EnterProjectName);
        String command = input.nextLine();
        for (Task task: projectService.getProjectTasks(command)) {
            System.out.println(task);
        }
    }

    private void displayHelp() {
        System.out.println("help: Show all commands");
        System.out.println("project-create: Create new project");
        System.out.println("project-clear: Remove all projects");
        System.out.println("project-list: Show all projects");
        System.out.println("project-remove: Remove selected project");
        System.out.println("task-clear: Remove all tasks");
        System.out.println("task-create: Create new task");
        System.out.println("task-list: Show all tasks");
        System.out.println("task-remove: Remove selected task");
        System.out.println("task-attach: attach task to project");
        System.out.println("project-tasks: display all tasks of project");
        System.out.println("exit: Exit");
    }
}
