package ru.lavrov.tm.сonstant;

public final class ConsoleConst {
    public static final String HELP = "help";
    public static final String CREATE_PROJECT = "project-create";
    public static final String CLEAR_PROJECT = "project-clear";
    public static final String DISPLAY_PROJECTS = "project-list";
    public static final String REMOVE_PROJECT = "project-remove";
    public static final String CLEAR_TASK = "task-clear";
    public static final String CREATE_TASK = "task-create";
    public static final String DISPLAY_TASK = "task-list";
    public static final String REMOVE_TASK = "task-remove";
    public static final String ATTACH_TASK = "task-attach";
    public static final String PROJECT_TASKS = "project-tasks";
    public static final String EXIT = "exit";
}
